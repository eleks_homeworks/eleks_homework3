﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Homework3.Implementations;
using Homework3.Models;

namespace Homework3
{
    static class Program
    {
        private const string FileName = "library.json";

        static void Main()
        {
            Library library = Library.CreateLibrary();
            Console.WriteLine(library);

            Console.WriteLine("Start of serialization");
            JsonSerializerHelper helper = new JsonSerializerHelper();
            helper.SerializeLibrary(FileName, library);
            Console.WriteLine($"Library serialized in file \"{FileName}\"");

            Console.WriteLine("Start of deserialization");
            Library deserializedLibrary = helper.DeserializeLibrary(FileName);
            Console.WriteLine($"Library deserialized from file \"{FileName}\"");
            Console.WriteLine(deserializedLibrary);

            Console.Read();
        }
    }
}
