﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Homework3.Interfaces;

namespace Homework3.Models
{
    public class Book : BaseBook
    {
        public Book() : base() { }

        public Book(string title, int pages) : base(title, pages) { }

        public override int CompareTo(BaseBook book)
        {
            if (Pages > book.Pages)
            {
                return 1;
            }
            else if (Pages < book.Pages)
            {
                return -1;
            }
            else
            {
                return 0;
            }
        }
    }
}
