﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Homework3.Interfaces;

namespace Homework3.Models
{
    public class Section : BaseSection
    {
        public Section() : base() { }

        public Section(string type, List<Author> authors) : base(type, authors) { }

        public override int CompareTo(BaseSection section)
        {
            if (GetBooksCount() > section.GetBooksCount())
            {
                return 1;
            }
            else if (GetBooksCount() < section.GetBooksCount())
            {
                return -1;
            }
            else
            {
                return 0;
            }
        }
    }
}
