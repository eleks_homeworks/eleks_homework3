﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Homework3.Interfaces;

namespace Homework3.Models
{
    public class Library : BaseLibrary
    {
        public Library() : base() {}

        public Library(List<Section> sections) : base(sections) { }

        public override int CompareTo(BaseLibrary library)
        {
            if (GetBooksCount() > library.GetBooksCount())
            {
                return 1;
            }
            else if (GetBooksCount() < library.GetBooksCount())
            {
                return -1;
            }
            else
            {
                return 0;
            }
        }

        // Additional tasks

        public void ShowBiggestSection()
        {
            int maxCount = Sections.Max(s => s.GetBooksCount());
            Section biggestSection = Sections.FirstOrDefault(s => s.GetBooksCount() == maxCount);

            Console.WriteLine(biggestSection == null ? "Error" :
                $"The biggest section is {biggestSection.Type}: {biggestSection.GetBooksCount()} books");
        }

        public void ShowHardworkingAuthor()
        {
            var authors = Sections.SelectMany(s => s.Authors);
            int maxCount = authors.Max(a => a.GetBooksCount());
            Author hardworkingAuthor = authors.FirstOrDefault(a => a.GetBooksCount() == maxCount);

            Console.WriteLine(hardworkingAuthor == null ? "Error" :
                $"The hardworking author is {hardworkingAuthor.Name}: {hardworkingAuthor.GetBooksCount()} books");
        }

        public void ShowThinnestBook()
        {
            var authors = Sections.SelectMany(s => s.Authors);
            var books = authors.SelectMany(a => a.Books);
            int minPagesCount = books.Min(b => b.Pages);
            Book thinnestBook = books.FirstOrDefault(b => b.Pages == minPagesCount);

            Console.WriteLine(thinnestBook == null ? "Error" :
                $"The thinnest book is {thinnestBook.Title}: {thinnestBook.Pages} pages");
        }

        public static Library CreateLibrary()
        {
            Author stehpenKing = new Author("Stephen King", new List<Book>()
            {
                new Book("11/22/63", 894),
                new Book("It", 1340),
                new Book("Joyland", 317),
                new Book("Revival", 413)
            });

            Author maxKidruk = new Author("Max Kidruk", new List<Book>()
            {
                new Book("Bot", 434),
                new Book("Stronghold", 390)
            });

            Section thrillers = new Section("Thillers", new List<Author>()
            {
                stehpenKing,
                maxKidruk
            });

            Author jeffSutherland = new Author("Jeff Sutherland", new List<Book>()
            {
                new Book("Scrum", 278)
            });

            Author jeffreyRichter = new Author("Jeffrey Richter", new List<Book>()
            {
                new Book("CLR via C#", 895)
            });

            Author steveMcConnell = new Author("Steve McConnell", new List<Book>()
            {
                new Book("Code complete", 880)
            });

            Section education = new Section("Education", new List<Author>()
            {
                jeffSutherland,
                jeffreyRichter,
                steveMcConnell
            });

            Library library = new Library(new List<Section>()
            {
                thrillers,
                education
            });

            return library;
        }
    }
}
