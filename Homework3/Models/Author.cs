﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Homework3.Interfaces;

namespace Homework3.Models
{
    public class Author : BaseAuthor
    {
        public Author() : base() { }

        public Author(string name, List<Book> books) : base(name, books) { }

        public override int CompareTo(BaseAuthor author)
        {
            if (GetBooksCount() > author.GetBooksCount())
            {
                return 1;
            }
            else if (GetBooksCount() < author.GetBooksCount())
            {
                return -1;
            }
            else
            {
                return 0;
            }
        }
    }
}
