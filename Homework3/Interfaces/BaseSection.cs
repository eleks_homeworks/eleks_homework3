﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Homework3.Models;

namespace Homework3.Interfaces
{
    public abstract class BaseSection : IComparable<BaseSection>, ICountingBooks
    {
        private List<Author> _authors;

        public string Type { get; set; }

        public List<Author> Authors => _authors ?? (_authors = new List<Author>());

        public BaseSection() { }

        public BaseSection(string type, List<Author> authors)
        {
            Type = type;
            _authors = authors;
        }

        public abstract int CompareTo(BaseSection section);

        public override string ToString()
        {
            StringBuilder builder = new StringBuilder();

            builder.Append($"The \"{Type}\" section has {GetBooksCount()} books total\n\n");
            
            var sortedAuthors = Authors.OrderBy(a => a.GetBooksCount());

            foreach (Author a in sortedAuthors)
            {
                builder.Append($"{a}\n");
            }
            return builder.ToString();
        }

        public int GetBooksCount() => Authors.Sum(a => a.GetBooksCount());
    }
}
