﻿using Homework3.Models;

namespace Homework3.Interfaces
{
    public interface IJsonSerializerHelper
    {
        void SerializeLibrary(string fileName, Library library);
        Library DeserializeLibrary(string fileName);
    }
}