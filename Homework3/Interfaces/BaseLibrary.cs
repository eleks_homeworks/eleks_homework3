﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Homework3.Models;

namespace Homework3.Interfaces
{
    public abstract class BaseLibrary : IComparable<BaseLibrary>, ICountingBooks
    {
        private List<Section> _sections;

        public List<Section> Sections => _sections ?? (_sections = new List<Section>());

        public BaseLibrary() { }

        public BaseLibrary(List<Section> sections)
        {
            _sections = sections;
        }

        public abstract int CompareTo(BaseLibrary library);

        public override string ToString()
        {
            StringBuilder builder = new StringBuilder();

            builder.Append($"The library has {GetBooksCount()} books total\n\n");

            var sortedSections = Sections.OrderBy(s => s.GetBooksCount());

            foreach (Section s in sortedSections)
            {
                builder.Append($"{s}\n");
            }
            return builder.ToString();
        }

        public int GetBooksCount() => Sections.Sum(s => s.GetBooksCount());
    }
}
