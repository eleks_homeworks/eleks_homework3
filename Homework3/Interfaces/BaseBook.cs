﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Homework3.Interfaces
{
    public abstract class BaseBook : IComparable<BaseBook>
    {
        public string Title { get; set; }

        public int Pages { get; set; }

        public BaseBook() { }

        public BaseBook(string title, int pages)
        {
            Title = title;
            Pages = pages;
        }

        public abstract int CompareTo(BaseBook book);

        public override string ToString() => $"\"{Title}\" with {Pages} pages";
    }
}
