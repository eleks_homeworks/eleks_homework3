﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Homework3.Models;

namespace Homework3.Interfaces
{
    public abstract class BaseAuthor : IComparable<BaseAuthor>, ICountingBooks
    {
        private List<Book> _books;

        public string Name { get; set; }

        public List<Book> Books => _books ?? (_books = new List<Book>());

        public BaseAuthor() { }

        public BaseAuthor(string name, List<Book> books)
        {
            Name = name;
            _books = books;
        }

        public abstract int CompareTo(BaseAuthor author);

        public override string ToString()
        {
            StringBuilder builder = new StringBuilder();
            builder.Append($"{Name} has written {Books.Count} books\n");

            var sortedBooks = Books.OrderBy(b => b.Pages);

            foreach (Book b in sortedBooks)
            {
                builder.Append($"{b}\n");
            }
            return builder.ToString();
        }

        public int GetBooksCount() => Books.Count;
    }
}
