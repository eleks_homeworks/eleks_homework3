﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Homework3.Interfaces;
using Homework3.Models;
using Newtonsoft.Json;

namespace Homework3.Implementations
{
    class JsonSerializerHelper : IJsonSerializerHelper
    {
        public void SerializeLibrary(string fileName, Library library)
        {
            using (StreamWriter writer = File.CreateText(fileName))
            {
                JsonSerializer serializer = new JsonSerializer();
                serializer.Serialize(writer, library);
            }
        }

        public Library DeserializeLibrary(string fileName)
        {
            string jsonString;
            using (StreamReader reader = File.OpenText(fileName))
            {
                jsonString = reader.ReadToEnd();
            }
            return JsonConvert.DeserializeObject<Library>(jsonString);
        }
    }
}
